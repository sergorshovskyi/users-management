package com.gxt.example.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.data.shared.StringLabelProvider;
import com.sencha.gxt.state.client.GridStateHandler;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.grid.CellSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridSelectionModel;
import com.sencha.gxt.widget.core.client.selection.CellSelectionChangedEvent;
import com.sencha.gxt.widget.core.client.selection.CellSelectionChangedEvent.CellSelectionChangedHandler;
import com.sencha.gxt.widget.core.client.toolbar.LabelToolItem;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class PersonDataGrid implements IsWidget {

	private HorizontalPanel hPanel;

	@Override
	public Widget asWidget() {
		if (hPanel == null) {
			ColumnConfig<Person, String> firstNameCol = new ColumnConfig<Person, String>(
					PersonDataContainer.properties.firstName(), 50, "Firs name");
			ColumnConfig<Person, String> lastNameCol = new ColumnConfig<Person, String>(
					PersonDataContainer.properties.lastName(), 50, "Last name");
			ColumnConfig<Person, String> sexCol = new ColumnConfig<Person, String>(PersonDataContainer.properties.sex(),
					50, "Sex");
			ColumnConfig<Person, Short> heightCol = new ColumnConfig<Person, Short>(
					PersonDataContainer.properties.height(), 50, "Height");
			ColumnConfig<Person, Date> birthdayCol = new ColumnConfig<Person, Date>(
					PersonDataContainer.properties.birthday(), 50, "Birthday");
			ColumnConfig<Person, String> countryCol = new ColumnConfig<Person, String>(
					PersonDataContainer.properties.country(), 50, "Country");
			ColumnConfig<Person, String> mailCol = new ColumnConfig<Person, String>(
					PersonDataContainer.properties.mail(), 50, "E-mail");
			ColumnConfig<Person, String> phoneCol = new ColumnConfig<Person, String>(
					PersonDataContainer.properties.phone(), 50, "Phone");

			List<ColumnConfig<Person, ?>> columns = new ArrayList<ColumnConfig<Person, ?>>();
			columns.add(firstNameCol);
			columns.add(lastNameCol);
			columns.add(sexCol);
			columns.add(heightCol);
			columns.add(birthdayCol);
			columns.add(countryCol);
			columns.add(mailCol);
			columns.add(phoneCol);

			ColumnModel<Person> cm = new ColumnModel<Person>(columns);

			final Grid<Person> grid = new Grid<Person>(GeneralContainer.personsStore, cm);
			grid.setAllowTextSelection(false);
			grid.getView().setStripeRows(true);
			grid.getView().setColumnLines(true);
			grid.setBorders(false);
			grid.setColumnReordering(true);
			Integer width = Window.getClientWidth();
			Integer height = Window.getClientHeight();
			grid.setSize(width.toString(), height.toString());

			// Stage manager, turn on state management
			grid.setStateful(true);
			grid.setStateId("gridExample");

			// Stage manager, load previous state
			GridStateHandler<Person> state = new GridStateHandler<Person>(grid);
			state.loadState();

			SimpleComboBox<String> typeCombo = new SimpleComboBox<String>(new StringLabelProvider<String>());
			typeCombo.setTriggerAction(TriggerAction.ALL);
			typeCombo.setEditable(false);
			typeCombo.setWidth(100);
			typeCombo.add("Row");
			typeCombo.add("Cell");
			typeCombo.setValue("Row");

			typeCombo.addSelectionHandler(new SelectionHandler<String>() {
				@Override
				public void onSelection(SelectionEvent<String> event) {
					boolean cell = event.getSelectedItem().equals("Cell");
					if (cell) {
						CellSelectionModel<Person> c = new CellSelectionModel<Person>();
						c.addCellSelectionChangedHandler(new CellSelectionChangedHandler<Person>() {
							@Override
							public void onCellSelectionChanged(CellSelectionChangedEvent<Person> event) {
							}
						});
						grid.setSelectionModel(c);
					} else {
						grid.setSelectionModel(new GridSelectionModel<Person>());
					}
				}
			});
			typeCombo.addValueChangeHandler(new ValueChangeHandler<String>() {
				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
				}
			});

			ToolBar toolBar = new ToolBar();
			toolBar.setEnableOverflow(false);
			toolBar.add(new LabelToolItem("Selection Mode: "));
			toolBar.add(typeCombo);

			VerticalLayoutContainer con = new VerticalLayoutContainer();
			con.add(toolBar, new VerticalLayoutData(1, -1));
			con.add(grid, new VerticalLayoutData(1, 1));

			hPanel = new HorizontalPanel();
			hPanel.add(con);
		}
		return hPanel;
	}

}
