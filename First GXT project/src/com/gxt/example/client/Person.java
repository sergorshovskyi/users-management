package com.gxt.example.client;

import java.io.Serializable;
import java.util.Date;

public class Person implements Serializable {

	private int id;
	private String firstName;
	private String lastName;
	private String sex;
	private short height;
	private String fullName;
	private Date birthday = new Date();
	private String country;
	private String mail;
	private String phone;

	public Person() {
	}

	public Person(int id, String firstName, String lastName, String sex, short height, String fullName, Date birthday,
			String country, String mail, String phone) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.height = height;
		this.fullName = fullName;
		this.birthday = birthday;
		this.country = country;
		this.mail = mail;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public short getHeight() {
		return height;
	}

	public void setHeight(short height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Person [fullName=" + fullName + ", sex=" + sex + ", height=" + height + ", birthday=" + birthday
				+ ", country=" + country + "]";
	}

}
