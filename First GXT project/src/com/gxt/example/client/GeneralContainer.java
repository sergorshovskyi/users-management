package com.gxt.example.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;

public class GeneralContainer implements IsWidget {

	private ContentPanel panel;
	private DataPanel contentPanel = new DataPanel();
	private PersonDataContainer personDataContainer = new PersonDataContainer();
	private PersonDataGrid personDataGrid = new PersonDataGrid();
	private int id = 0;
	public static ListStore<Person> personsStore = new ListStore<Person>(PersonDataContainer.properties.key());

	@Override
	public Widget asWidget() {
		if (panel == null) {

			HorizontalPanel hPanel = new HorizontalPanel();
			TextButton saveButton = new TextButton("Save");
			TextButton canselButton = new TextButton("Cansel");
			hPanel.add(saveButton);
			hPanel.add(canselButton);
			hPanel.setSpacing(10);

			VerticalLayoutContainer vlc = new VerticalLayoutContainer();
			vlc.add(contentPanel.asWidget());
			vlc.add(hPanel);
			VerticalLayoutContainer vlc1 = new VerticalLayoutContainer();
			vlc1.add(personDataContainer.asWidget());
			VerticalLayoutContainer vlc2 = new VerticalLayoutContainer();
			vlc2.add(personDataGrid.asWidget());

			TabPanel folderTabPanel = new TabPanel();
			folderTabPanel.add(vlc, "Add person");
			folderTabPanel.add(vlc1, "Persons | ListView");
			folderTabPanel.add(vlc2, "Persons | Grid");

			saveButton.addSelectHandler(new SelectHandler() {

				@Override
				public void onSelect(SelectEvent event) {
					if (contentPanel.getFirstNameTextField().getText().isEmpty()
							|| contentPanel.getLastNameTextField().getText().isEmpty()
							|| contentPanel.getPhoneTextField().getText().isEmpty()
							|| contentPanel.getPhoneTextField().getText().isEmpty()) {
						Info.display("Empty field", "None of the fields should be empty");
						return;
					}
					if (contentPanel.getRadioMale().getValue().equals(false)
							&& contentPanel.getRadioFemale().getValue().equals(false)) {
						Info.display("Invalid credentials", "Please, chose your sex");
						return;
					}
					if (!contentPanel.getFirstNameTextField().isValid()
							|| !contentPanel.getLastNameTextField().isValid()
							|| !contentPanel.getMailTextField().isValid()
							|| !contentPanel.getPhoneTextField().isValid()) {
						return;
					}

					Person person = new Person();
					person.setId(id);
					person.setHeight(contentPanel.getHeightSpinnerField().getValue());
					person.setFirstName(contentPanel.getFirstNameTextField().getText());
					person.setLastName(contentPanel.getLastNameTextField().getText());
					if (contentPanel.getRadioMale().getValue().equals(true)) {
						person.setSex("Male");
					} else if (contentPanel.getRadioFemale().getValue().equals(true)) {
						person.setSex("Female");
					}
					person.setFullName(person.getFirstName() + " " + person.getLastName());
					person.setBirthday(contentPanel.getBirthsdayField().getCurrentValue());
					person.setCountry(contentPanel.getCountryComboBox().getValue());
					person.setMail(contentPanel.getMailTextField().getText());
					person.setPhone(contentPanel.getPhoneTextField().getText());
					First_GXT_project.greetingService.addPersonToFile(person, new AsyncCallback<String>() {

						@Override
						public void onSuccess(String result) {
							Info.display(result, "Person successfully added");
						}

						@Override
						public void onFailure(Throwable caught) {
							Info.display("Failure", caught.toString());
						}
					});
					personsStore.add(person);
					id++;
					contentPanel.getFirstNameTextField().setText("");
					contentPanel.getLastNameTextField().setText("");
					contentPanel.getBirthsdayField().clear();
					contentPanel.getCountryComboBox().setValue("");
					contentPanel.getRadioMale().setValue(false);
					contentPanel.getRadioFemale().setValue(false);
					contentPanel.getHeightSpinnerField().setValue(Short.parseShort("170"));
					contentPanel.getMailTextField().setText("");
					contentPanel.getPhoneTextField().setText("");
				}
			});

			canselButton.addSelectHandler(new SelectHandler() {

				@Override
				public void onSelect(SelectEvent event) {
					contentPanel.getFirstNameTextField().setText("");
					contentPanel.getLastNameTextField().setText("");
					contentPanel.getBirthsdayField().clear();
					contentPanel.getCountryComboBox().setValue("");
					contentPanel.getRadioMale().setValue(false);
					contentPanel.getRadioFemale().setValue(false);
					contentPanel.getHeightSpinnerField().setValue(Short.parseShort("170"));
					contentPanel.getPhoneTextField().setText("");
					contentPanel.getMailTextField().setText("");
				}
			});

			panel = new ContentPanel();
			panel.setHeading("Users Management");
			panel.add(folderTabPanel);
		}

		return panel;
	}

	public static void updatePerson(Person oldPerson, Person newPerson) {
		First_GXT_project.greetingService.updatePersonInFile(oldPerson, newPerson, new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				Info.display("MessageBox", "Person updated");
			}

			@Override
			public void onFailure(Throwable caught) {
				Info.display("Failure", caught.toString());
			}
		});
	}

}
