package com.gxt.example.client;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.Radio;
import com.sencha.gxt.widget.core.client.form.ShortSpinnerField;
import com.sencha.gxt.widget.core.client.form.StringComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;

public class DataPanel implements IsWidget {

	private TextField textFieldForFirstName;
	private TextField textFieldForLastName;
	private Radio radioMale;
	private Radio radioFemale;
	private ShortSpinnerField shortSpinnerField;
	private DateField birthsdayField;
	private VerticalLayoutContainer vlc;
	private StringComboBox countryComboBox;
	private TextButton saveButton;
	private TextButton deleteButton;
	private TextField textFieldForMail;
	private TextField textFieldForPhone;

	@Override
	public Widget asWidget() {
		if (vlc == null) {
			vlc = new VerticalLayoutContainer();
			vlc.add(new FieldLabel(getFirstNameTextField(), "First name"));
			vlc.add(new FieldLabel(getLastNameTextField(), "Last name"));
			ToggleGroup toggleGroup = new ToggleGroup();
			toggleGroup.add(getRadioMale());
			toggleGroup.add(getRadioFemale());
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.add(getRadioMale());
			horizontalPanel.add(getRadioFemale());
			vlc.add(new FieldLabel(horizontalPanel, "Sex"));
			vlc.add(new FieldLabel(getHeightSpinnerField(), "Height"));
			vlc.add(new FieldLabel(getBirthsdayField(), "Birthsday"));
			vlc.add(new FieldLabel(getCountryComboBox(), "Country"));
			vlc.add(new FieldLabel(getMailTextField(), "E-mail"));
			vlc.add(new FieldLabel(getPhoneTextField(), "Phone number"));

			HorizontalPanel horizontalPanelForButtons = new HorizontalPanel();
			horizontalPanelForButtons.add(getSaveButton());
			horizontalPanelForButtons.add(getDeleteButton());
			horizontalPanelForButtons.setSpacing(5);

			vlc.add(horizontalPanelForButtons);

			getDeleteButton().setVisible(false);
			getSaveButton().setVisible(false);
		}
		return vlc;
	}

	public TextButton getDeleteButton() {
		if (deleteButton == null) {
			deleteButton = new TextButton("Delete");
		}
		return deleteButton;
	}

	public TextButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new TextButton("Save");
		}
		return saveButton;
	}

	public Radio getRadioMale() {
		if (radioMale == null) {
			radioMale = new Radio();
			radioMale.setBoxLabel("Male");
		}
		return radioMale;
	}

	public Radio getRadioFemale() {
		if (radioFemale == null) {
			radioFemale = new Radio();
			radioFemale.setBoxLabel("Female");
		}
		return radioFemale;
	}

	public ShortSpinnerField getHeightSpinnerField() {
		if (shortSpinnerField == null) {
			shortSpinnerField = new ShortSpinnerField();
			shortSpinnerField.setValue(Short.parseShort("170"));
			shortSpinnerField.setAllowNegative(false);
		}
		return shortSpinnerField;
	}

	public TextField getFirstNameTextField() {
		if (textFieldForFirstName == null) {
			textFieldForFirstName = new TextField();
			textFieldForFirstName.addValidator(new RegExValidator("^[a-zA-Z\\.]{2,30}$", "Not a valid first name."));
		}
		return textFieldForFirstName;
	}

	public TextField getMailTextField() {
		if (textFieldForMail == null) {
			textFieldForMail = new TextField();
			textFieldForMail.addValidator(
					new RegExValidator("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$", "Not a valid e-mail."));
			textFieldForMail.setEmptyText("example@mail.com");
		}
		return textFieldForMail;
	}

	public TextField getPhoneTextField() {
		if (textFieldForPhone == null) {
			textFieldForPhone = new TextField();
			textFieldForPhone.addValidator(new RegExValidator("^(0)\\d{9}$", "Not a valid phone."));
			textFieldForPhone.setEmptyText("0123456789");
		}
		return textFieldForPhone;
	}

	public TextField getLastNameTextField() {
		if (textFieldForLastName == null) {
			textFieldForLastName = new TextField();
			textFieldForLastName.addValidator(new RegExValidator("^[a-zA-Z\\.]{2,30}$", "Not a valid last name."));
		}
		return textFieldForLastName;
	}

	public DateField getBirthsdayField() {
		if (birthsdayField == null) {
			birthsdayField = new DateField();
		}
		return birthsdayField;
	}

	public StringComboBox getCountryComboBox() {
		if (countryComboBox == null) {
			countryComboBox = new StringComboBox();
			countryComboBox.add("Ukraine");
			countryComboBox.add("Romania");
			countryComboBox.add("USA");
			countryComboBox.setEmptyText("Select a country");
		}
		return countryComboBox;
	}

}
