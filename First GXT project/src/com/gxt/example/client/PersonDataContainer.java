package com.gxt.example.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.ListView;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent.SelectionChangedHandler;

public class PersonDataContainer implements IsWidget {

	public static PersonProperties properties = GWT.create(PersonProperties.class);

	private ListView<Person, String> usersListView;

	private DataPanel contentPanel = new DataPanel();
	private Person person = new Person();

	@Override
	public Widget asWidget() {
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.add(getListView());
		hPanel.add(contentPanel.asWidget());
		hPanel.setSpacing(10);
		contentPanel.getSaveButton().setVisible(true);
		contentPanel.getDeleteButton().setVisible(true);
		return hPanel;
	}

	private ListView<Person, String> getListView() {
		if (usersListView == null) {
			usersListView = new ListView<Person, String>(GeneralContainer.personsStore, properties.fullName());
			usersListView.setSize("200", "400");
			usersListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

			usersListView.getSelectionModel().addSelectionChangedHandler(new SelectionChangedHandler<Person>() {

				@Override
				public void onSelectionChanged(SelectionChangedEvent<Person> person) {
					if (person.getSelection().size() > 0) {
						contentPanel.getFirstNameTextField().setText(person.getSelection().get(0).getFirstName());
						contentPanel.getLastNameTextField().setText(person.getSelection().get(0).getLastName());
						if (person.getSelection().get(0).getSex().equals("Male")) {
							contentPanel.getRadioMale().setValue(true);
							contentPanel.getRadioFemale().setValue(false);
						} else if (person.getSelection().get(0).getSex().equals("Female")) {
							contentPanel.getRadioFemale().setValue(true);
							contentPanel.getRadioMale().setValue(false);
						}
						contentPanel.getHeightSpinnerField().setValue(person.getSelection().get(0).getHeight());
						contentPanel.getBirthsdayField().setValue(person.getSelection().get(0).getBirthday());
						contentPanel.getCountryComboBox().setValue(person.getSelection().get(0).getCountry());
						contentPanel.getMailTextField().setText(person.getSelection().get(0).getMail());
						contentPanel.getPhoneTextField().setText(person.getSelection().get(0).getPhone());

						contentPanel.getSaveButton().addSelectHandler(new SelectHandler() {

							@Override
							public void onSelect(SelectEvent event) {

								if (contentPanel.getFirstNameTextField().getText().isEmpty()
										|| contentPanel.getLastNameTextField().getText().isEmpty()
										|| contentPanel.getPhoneTextField().getText().isEmpty()
										|| contentPanel.getPhoneTextField().getText().isEmpty()) {
									Info.display("Empty field", "None of the fields should be empty");
									return;
								}
								if (contentPanel.getRadioMale().getValue().equals(false)
										&& contentPanel.getRadioFemale().getValue().equals(false)) {
									Info.display("Invalid credentials", "Please, chose your sex");
									return;
								}
								if (!contentPanel.getFirstNameTextField().isValid()
										|| !contentPanel.getLastNameTextField().isValid()
										|| !contentPanel.getMailTextField().isValid()
										|| !contentPanel.getPhoneTextField().isValid()) {
									return;
								}
								MessageBox messageBox = new MessageBox("Confirmation", "");
								messageBox.setPredefinedButtons(PredefinedButton.YES, PredefinedButton.NO);
								messageBox.setIcon(MessageBox.ICONS.question());
								messageBox.setMessage(
										"Do you want to update " + person.getSelection().get(0).getFullName() + "?");
								messageBox.addDialogHideHandler(new DialogHideHandler() {
									@Override
									public void onDialogHide(DialogHideEvent event) {

										switch (event.getHideButton()) {
										case YES:
											Person oldPerson = new Person();

											oldPerson.setId(person.getSelection().get(0).getId());
											oldPerson.setFirstName(person.getSelection().get(0).getFirstName());
											oldPerson.setLastName(person.getSelection().get(0).getLastName());
											oldPerson.setFullName(person.getSelection().get(0).getFullName());
											oldPerson.setSex(person.getSelection().get(0).getSex());
											oldPerson.setHeight(person.getSelection().get(0).getHeight());
											oldPerson.setBirthday(person.getSelection().get(0).getBirthday());
											oldPerson.setCountry(person.getSelection().get(0).getCountry());
											oldPerson.setMail(person.getSelection().get(0).getMail());
											oldPerson.setPhone(person.getSelection().get(0).getPhone());

											person.getSelection().get(0)
													.setFirstName(contentPanel.getFirstNameTextField().getText());
											person.getSelection().get(0)
													.setLastName(contentPanel.getLastNameTextField().getText());
											person.getSelection().get(0)
													.setBirthday(contentPanel.getBirthsdayField().getValue());
											person.getSelection().get(0)
													.setCountry(contentPanel.getCountryComboBox().getValue());
											person.getSelection().get(0)
													.setHeight(contentPanel.getHeightSpinnerField().getValue());
											if (contentPanel.getRadioMale().getValue()) {
												person.getSelection().get(0).setSex("Male");
											} else if (contentPanel.getRadioFemale().getValue()) {
												person.getSelection().get(0).setSex("Female");
											}
											person.getSelection().get(0)
													.setFullName(person.getSelection().get(0).getFirstName() + " "
															+ person.getSelection().get(0).getLastName());
											person.getSelection().get(0)
													.setMail(contentPanel.getMailTextField().getText());
											person.getSelection().get(0)
													.setPhone(contentPanel.getPhoneTextField().getText());

											Person newPerson = new Person();
											newPerson.setId(person.getSelection().get(0).getId());
											newPerson.setFirstName(person.getSelection().get(0).getFirstName());
											newPerson.setLastName(person.getSelection().get(0).getLastName());
											newPerson.setFullName(person.getSelection().get(0).getFullName());
											newPerson.setSex(person.getSelection().get(0).getSex());
											newPerson.setHeight(person.getSelection().get(0).getHeight());
											newPerson.setBirthday(person.getSelection().get(0).getBirthday());
											newPerson.setCountry(person.getSelection().get(0).getCountry());
											newPerson.setMail(person.getSelection().get(0).getMail());
											newPerson.setPhone(person.getSelection().get(0).getPhone());

											GeneralContainer.updatePerson(oldPerson, newPerson);
											usersListView.getStore().update(person.getSelection().get(0));
											break;
										}
									}
								});
								messageBox.show();

							}
						});

						contentPanel.getDeleteButton().addSelectHandler(new SelectHandler() {

							@Override
							public void onSelect(SelectEvent event) {
								MessageBox messageBox = new MessageBox("Confirmation", "");
								messageBox.setPredefinedButtons(PredefinedButton.YES, PredefinedButton.NO);
								messageBox.setIcon(MessageBox.ICONS.question());
								messageBox.setMessage(
										"Do you want to delete " + person.getSelection().get(0).getFullName() + "?");
								messageBox.addDialogHideHandler(new DialogHideHandler() {
									@Override
									public void onDialogHide(DialogHideEvent event) {

										switch (event.getHideButton()) {
										case YES:
											Person personForRemove = new Person();
											personForRemove.setId(person.getSelection().get(0).getId());
											personForRemove.setFirstName(person.getSelection().get(0).getFirstName());
											personForRemove.setLastName(person.getSelection().get(0).getLastName());
											personForRemove.setFullName(person.getSelection().get(0).getFullName());
											personForRemove.setSex(person.getSelection().get(0).getSex());
											personForRemove.setHeight(person.getSelection().get(0).getHeight());
											personForRemove.setBirthday(person.getSelection().get(0).getBirthday());
											personForRemove.setCountry(person.getSelection().get(0).getCountry());
											personForRemove.setMail(person.getSelection().get(0).getMail());
											personForRemove.setPhone(person.getSelection().get(0).getPhone());
											First_GXT_project.greetingService.deletePersonFromFile(personForRemove,
													new AsyncCallback<String>() {

														@Override
														public void onSuccess(String result) {

														}

														@Override
														public void onFailure(Throwable caught) {
															Info.display("Failure", caught.toString());
														}
													});

											usersListView.getStore().remove(person.getSelection().get(0));
											contentPanel.getFirstNameTextField().setText("");
											contentPanel.getLastNameTextField().setText("");
											contentPanel.getBirthsdayField().clear();
											contentPanel.getCountryComboBox().setValue("");
											contentPanel.getRadioMale().setValue(false);
											contentPanel.getRadioFemale().setValue(false);
											contentPanel.getHeightSpinnerField().setValue(Short.parseShort("170"));
											contentPanel.getPhoneTextField().setText("");
											contentPanel.getMailTextField().setText("");
											Info.display("MessageBox", "Person deleted");
											break;
										}
									}
								});
								messageBox.show();

							}
						});
					}
				}
			});

		}
		return usersListView;
	}

}
