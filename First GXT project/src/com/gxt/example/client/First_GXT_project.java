package com.gxt.example.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

public class First_GXT_project implements EntryPoint {

	public static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	@Override
	public void onModuleLoad() {
		GeneralContainer generalContainer = new GeneralContainer();
		RootPanel.get().add(generalContainer.asWidget());
	}
}
