package com.gxt.example.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {

	void addPersonToFile(Person person, AsyncCallback<String> callback);

	void updatePersonInFile(Person oldPerson, Person newPerson, AsyncCallback<String> callback);

	void deletePersonFromFile(Person person, AsyncCallback<String> callback);

}
