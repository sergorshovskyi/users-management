package com.gxt.example.client;

import java.util.Date;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PersonProperties extends PropertyAccess<Person> {
	@Path("id")
	ModelKeyProvider<Person> key();

	@Path("fullName")
	ValueProvider<Person, String> fullName();

	ValueProvider<Person, String> firstName();

	ValueProvider<Person, String> lastName();

	ValueProvider<Person, String> sex();

	ValueProvider<Person, Short> height();

	ValueProvider<Person, Date> birthday();

	ValueProvider<Person, String> country();

	ValueProvider<Person, String> mail();

	ValueProvider<Person, String> phone();
}
