package com.gxt.example.client;

import java.io.IOException;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String addPersonToFile(Person person) throws IOException;

	String updatePersonInFile(Person oldPerson, Person newPerson);

	String deletePersonFromFile(Person person);
}
