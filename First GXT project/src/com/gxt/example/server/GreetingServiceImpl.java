package com.gxt.example.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.gxt.example.client.GreetingService;
import com.gxt.example.client.Person;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	@Override
	public String addPersonToFile(Person person) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\User\\eclipse-workspace\\persons.txt", true));

		bw.append("{ ");
		bw.append("id: " + person.getId() + " ");
		bw.append("name: " + person.getFullName() + " ");
		bw.append("sex: " + person.getSex() + " ");
		bw.append("height: " + person.getHeight() + " ");
		bw.append("birthday: " + person.getBirthday() + " ");
		bw.append("country: " + person.getCountry() + " ");
		bw.append("e-mail: " + person.getMail() + " ");
		bw.append("phone: " + person.getPhone() + " ");
		bw.append("}");
		bw.newLine();
		bw.close();
		return "Success";
	}

	@Override
	public String updatePersonInFile(Person oldPerson, Person newPerson) {
		File persons = new File("D:\\User\\eclipse-workspace\\persons.txt");
		try {
			File temp = File.createTempFile("persons", ".txt", persons.getParentFile());
			String charset = "UTF-8";
			String oldInfo = "{ id: " + oldPerson.getId() + " name: " + oldPerson.getFullName() + " sex: "
					+ oldPerson.getSex() + " height: " + oldPerson.getHeight() + " birthday: " + oldPerson.getBirthday()
					+ " country: " + oldPerson.getCountry() + " e-mail: " + oldPerson.getMail() + " phone: "
					+ oldPerson.getPhone() + " }";
			String newInfo = "{ id: " + newPerson.getId() + " name: " + newPerson.getFullName() + " sex: "
					+ newPerson.getSex() + " height: " + newPerson.getHeight() + " birthday: " + newPerson.getBirthday()
					+ " country: " + newPerson.getCountry() + " e-mail: " + newPerson.getMail() + " phone: "
					+ newPerson.getPhone() + " }";
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(persons), charset));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
			for (String line; (line = reader.readLine()) != null;) {
				line = line.replace(oldInfo, newInfo);
				writer.println(line);
			}
			reader.close();
			writer.close();
			persons.delete();
			temp.renameTo(persons);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String deletePersonFromFile(Person person) {
		File persons = new File("D:\\User\\eclipse-workspace\\persons.txt");
		try {
			File temp = File.createTempFile("persons", ".txt", persons.getParentFile());
			String charset = "UTF-8";
			String delete = "{ id: " + person.getId() + " name: " + person.getFullName() + " sex: " + person.getSex()
					+ " height: " + person.getHeight() + " birthday: " + person.getBirthday() + " country: "
					+ person.getCountry() + " e-mail: " + person.getMail() + " phone: " + person.getPhone() + " }";
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(persons), charset));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
			for (String line; (line = reader.readLine()) != null;) {
				line = line.replace(delete, "");
				writer.println(line);
			}
			reader.close();
			writer.close();
			persons.delete();
			temp.renameTo(persons);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "";
	}

}
